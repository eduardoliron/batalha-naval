// See https://kit.svelte.dev/docs/types#app
// for information about these interfaces
declare global {
  namespace App {
    // interface Error {}
    // interface Locals {}
    // interface PageData {}
    // interface Platform {}
  }
}

type GameEvent = PlayerId
  | PlayerJoin
  | PlayerChange
  | PlayerWaiting
  | GameInit
  | GameStarted
  | GamePlay
  | GameEnded
  | GameError
  | ListRooms

interface PlayerId { "type": "player_id", "player_id": string, "num_players": number }
interface PlayerJoin { "type": "player_joined", "num_players": number }
interface PlayerChange { "type": "next_player", "player_id": string }
interface PlayerWaiting { "type": "wait_your_turn" }
interface GameInit {
  "type": "init"
  "join": string
  "watch": string
}
interface GameStarted { "type": "game_started", "next_turn": string }
interface GamePlay {
  "type": "play"
  "player": string
  "column": number
  "row": number
  "result": shootResult
}
interface GameEnded { "type": "won" }
interface GameError {
  "type": "error"
  "message": message
}
interface ListRooms {
  "type": "list-rooms"
  "rooms": number[]
}

interface ShootResult {
  result: [string, number]
  grid: [number, number]
  next_turn: string
}

interface ShipModel {
  order: number
  name: string
  date: string
  image: string
  shape: number[]
  destroyed: boolean
  info: ShipInfo
}

interface ShipInfo {
  type: string
  flag: string
  route: string
  time: number
  quant: number
  departed: number
  dead: number
  arrived: number
  text?: string
}

interface Particle {
  x: number
  y: number
  radius: number
  rotation: number
  speed: number
  friction: number
  opacity: number
  yVel: number
  gravity: number
}