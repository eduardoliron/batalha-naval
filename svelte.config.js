import adapter from '@sveltejs/adapter-static';
import { vitePreprocess } from '@sveltejs/kit/vite';

/** @type {import('@sveltejs/kit').Config} */
const config = {
  kit: {
    adapter: adapter({
      pages: 'public',
      assets: 'public',
      fallback: 'index.html'
    }),
    alias: {
      '$data': 'src/data',
      '$data/*': 'src/data/*'
    }
  },
  preprocess: vitePreprocess()
};

export default config;